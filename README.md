# 🥁 &nbsp; [Beatoftheday.org](https://www.beatoftheday.org/) &nbsp; 🌞

### Getting Started

- `nvm use`
- `yarn`
- `ruby -v` should equal `cat .ruby-version`

- `brew bundle` if on mac OS or install redis
- `brew services restart redis`

- `bundle`
- `bundle exec rails db:create db:migrate db:seed`
- `bundle exec rails server`

